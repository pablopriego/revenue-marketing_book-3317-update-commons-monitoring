package com.odigeo.marketing.rmarketing.revenuemodels;

import java.util.Objects;

public class CancellationRevenue implements Revenue {

    String dateRequest;
    String edoBrand;
    String edoMarket;
    String itineraryHaulType;
    String descItineraryHaulType;
    String productDetail;
    String revenue;


    public CancellationRevenue(String dateRequest, String edoBrand, String edoMarket, String itineraryHaulType, String descItineraryHaulType, String productDetail, String cancellationIssued) {
        this.dateRequest = dateRequest;
        this.edoBrand = edoBrand;
        this.edoMarket = edoMarket;
        this.itineraryHaulType = itineraryHaulType;
        this.descItineraryHaulType = descItineraryHaulType;
        this.productDetail = productDetail;
        this.revenue = cancellationIssued;
    }

    public String getDateRequest() {
        return dateRequest;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CancellationRevenue that = (CancellationRevenue) o;
        return Objects.equals(edoBrand, that.edoBrand)
                && Objects.equals(edoMarket, that.edoMarket)
                && Objects.equals(itineraryHaulType, that.itineraryHaulType)
                && Objects.equals(productDetail, that.productDetail);
    }

    @Override
    public int hashCode() {
        return Objects.hash(edoBrand, edoMarket, itineraryHaulType, productDetail);
    }

    public String getEdoBrand() {
        return edoBrand;
    }

    public String getEdoMarket() {
        return edoMarket;
    }

    public String getItineraryHaulType() {
        return itineraryHaulType;
    }

    public String getDescItineraryHaulType() {
        return descItineraryHaulType;
    }

    public String getProductDetail() {
        return productDetail;
    }

    @Override
    public String getRevenueValue() {
        return revenue;
    }
}

