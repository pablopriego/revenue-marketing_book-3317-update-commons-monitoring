package com.odigeo.marketing.rmarketing.revenuemodels;

import java.util.Objects;

public class CCIncentiveRevenue implements Revenue {

    String dateRequest;
    String edoBrand;
    String edoMarket;
    String ccIncentiveIssued;

    public CCIncentiveRevenue(String dateRequest, String edoBrand, String edoMarket, String ccIncentiveIssued) {
        this.dateRequest = dateRequest;
        this.edoBrand = edoBrand;
        this.edoMarket = edoMarket;
        this.ccIncentiveIssued = ccIncentiveIssued;
    }

    public String getDateRequest() {
        return dateRequest;
    }

    public String getEdoBrand() {
        return edoBrand;
    }

    public String getEdoMarket() {
        return edoMarket;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CCIncentiveRevenue that = (CCIncentiveRevenue) o;
        return Objects.equals(edoBrand, that.edoBrand)
                &&
                Objects.equals(edoMarket, that.edoMarket);
    }

    @Override
    public int hashCode() {
        return Objects.hash(edoBrand, edoMarket);
    }

    @Override
    public String getRevenueValue() {
        return ccIncentiveIssued;
    }
}
