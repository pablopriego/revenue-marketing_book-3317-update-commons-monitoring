package com.odigeo.marketing.rmarketing.revenuemodels;

import java.util.Objects;

public class TaxRefundRevenue implements Revenue {

    String dateRequest;
    String edoWebsite;
    String itineraryHaulType;
    String descItineraryHaulType;
    String productDetail;
    String provider;
    String carrierValidating;
    String taxRefundIssued;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TaxRefundRevenue that = (TaxRefundRevenue) o;
        return Objects.equals(edoWebsite, that.edoWebsite)
                && Objects.equals(itineraryHaulType, that.itineraryHaulType)
                && Objects.equals(productDetail, that.productDetail)
                && Objects.equals(provider, that.provider)
                && Objects.equals(carrierValidating, that.carrierValidating);
    }

    @Override
    public int hashCode() {
        return Objects.hash(edoWebsite, itineraryHaulType, productDetail, provider, carrierValidating);
    }

    public TaxRefundRevenue(String dateRequest, String edoWebsite, String itineraryHaulType, String descItineraryHaulType, String productDetail, String provider, String carrierValidating, String taxRefundIssued) {
        this.dateRequest = dateRequest;
        this.edoWebsite = edoWebsite;
        this.itineraryHaulType = itineraryHaulType;
        this.descItineraryHaulType = descItineraryHaulType;
        this.productDetail = productDetail;
        this.provider = provider;
        this.carrierValidating = carrierValidating;
        this.taxRefundIssued = taxRefundIssued;
    }


    public String getDateRequest() {
        return dateRequest;
    }

    public String getEdoWebsite() {
        return edoWebsite;
    }

    public String getItineraryHaulType() {
        return itineraryHaulType;
    }

    public String getProductDetail() {
        return productDetail;
    }

    public String getProvider() {
        return provider;
    }

    public String getCarrierValidating() {
        return carrierValidating;
    }

    public String getDescItineraryHaulType() {
        return descItineraryHaulType;
    }

    @Override
    public String getRevenueValue() {
        return taxRefundIssued;
    }
}
