package com.odigeo.marketing.rmarketing.revenuemodels;

import java.util.Objects;

public class OtherRevenue implements Revenue {

    String dateRequest; //test
    String descShortRequestDay;
    String edoBrand;
    String descBrandAbbreviation;
    String edoMarket;
    String descMarket;
    String otherIssued;

    public OtherRevenue(String dateRequest, String descShortRequestDay, String edoBrand, String descBrandAbbreviation, String edoMarket, String descMarket, String otherIssued) {
        this.dateRequest = dateRequest;
        this.descShortRequestDay = descShortRequestDay;
        this.edoBrand = edoBrand;
        this.descBrandAbbreviation = descBrandAbbreviation;
        this.edoMarket = edoMarket;
        this.descMarket = descMarket;
        this.otherIssued = otherIssued;
    }

    public String getDateRequest() {
        return dateRequest;
    }

    public String getEdoBrand() {
        return edoBrand;
    }

    public String getEdoMarket() {
        return edoMarket;
    }

    public String getDescShortRequestDay() {
        return descShortRequestDay;
    }

    public String getDescBrandAbbreviation() {
        return descBrandAbbreviation;
    }

    public String getDescMarket() {
        return descMarket;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OtherRevenue that = (OtherRevenue) o;
        return Objects.equals(edoBrand, that.edoBrand)
                && Objects.equals(edoMarket, that.edoMarket);
    }

    @Override
    public int hashCode() {
        return Objects.hash(edoBrand, edoMarket);
    }

    @Override
    public String getRevenueValue() {
        return otherIssued;
    }
}
