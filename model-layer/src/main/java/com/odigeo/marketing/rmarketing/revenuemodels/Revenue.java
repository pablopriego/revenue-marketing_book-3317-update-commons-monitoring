package com.odigeo.marketing.rmarketing.revenuemodels;

public interface Revenue {
    
    String getRevenueValue();


}
