package com.odigeo.marketing.revenueservice;

import com.edreams.configuration.ConfigurationEngine;
import com.google.cloud.bigquery.TableResult;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.marketing.revenue.bigquery.BigQueryController;
import com.odigeo.marketing.rmarketing.v1.BookingDetailsContainer;
import com.odigeo.marketing.rmarketing.v1.response.RevenueMarketingAdjustmentContainer;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

@Singleton
public class RevenueManager {

    List<RevenueController> revenueControllers;
    BigQueryController bigQueryController;
    CacheStatus cacheStatus;

    private static final Logger LOGGER = Logger.getLogger(RevenueManager.class);

    CancellationRevenueController cancellationRevenueController;
    CCIncentiveRevenueController ccIncentiveRevenueController;
    OtherRevenueController otherRevenueController;
    TaxRefundRevenueController taxRefundRevenueController;

    @Inject
    public RevenueManager(BigQueryController bigQueryController, CacheStatus cacheStatus, CancellationRevenueController cancellationRevenueController,     CCIncentiveRevenueController ccIncentiveRevenueController,     OtherRevenueController otherRevenueController,     TaxRefundRevenueController taxRefundRevenueController) {
        this.cacheStatus = cacheStatus;
        this.cancellationRevenueController = cancellationRevenueController;
        this.ccIncentiveRevenueController = ccIncentiveRevenueController;
        this.otherRevenueController = otherRevenueController;
        this.taxRefundRevenueController = taxRefundRevenueController;
        this.bigQueryController = bigQueryController;
    }

    public RevenueMarketingAdjustmentContainer getRevenue(BookingDetailsContainer bookingDetailsContainer) {
        checkCurrentCacheStatus();

        return new RevenueMarketingAdjustmentContainer(cancellationRevenueController.getCancellationAmount(bookingDetailsContainer),
                ccIncentiveRevenueController.getCCIncentiveAmount(bookingDetailsContainer),
                otherRevenueController.getOtherRevenueAmount(bookingDetailsContainer),
                taxRefundRevenueController.getTaxRefundAmount(bookingDetailsContainer));
    }

    private void checkCurrentCacheStatus() {
        CacheStatus cacheStatus = ConfigurationEngine.getInstance(CacheStatus.class);
        if (cacheStatus.getAndSetCurrentCacheStatus() == 0) {
            update();
        }
    }


    private void initControllers() {
        if (revenueControllers == null) {
            revenueControllers = new ArrayList<>();
            revenueControllers.add(cancellationRevenueController);
            revenueControllers.add(ccIncentiveRevenueController);
            revenueControllers.add(otherRevenueController);
            revenueControllers.add(taxRefundRevenueController);
        }
    }

    private void updateRevenueCaches() {
        for (RevenueController revenueController : revenueControllers) {
            try {
                TableResult result = bigQueryController.getResults(revenueController.getQuery());
                if (result != null) {
                    revenueController.update(result);
                }
            } catch (InterruptedException e) {
                LOGGER.info("Unable to update revenue cache for: " + revenueController.getMapper().toString() + " :" + e.getMessage());
            }
        }
    }

    private void update() {
        bigQueryController.getService();
        initControllers();
        updateRevenueCaches();
    }
}
