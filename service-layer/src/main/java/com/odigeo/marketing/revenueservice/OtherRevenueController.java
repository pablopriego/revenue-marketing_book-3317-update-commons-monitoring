package com.odigeo.marketing.revenueservice;

import com.google.cloud.bigquery.FieldValueList;
import com.google.inject.Singleton;
import com.odigeo.marketing.rmarketing.revenuemodels.OtherRevenue;
import com.odigeo.marketing.rmarketing.revenuemodels.Revenue;
import com.odigeo.marketing.rmarketing.v1.BookingDetailsContainer;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

@Singleton
public class OtherRevenueController extends RevenueController {

    private final Function<FieldValueList, Revenue> mapperFunction = (FieldValueList row) -> mapRow(row);

    public Function<FieldValueList, Revenue> getMapper() {
        return mapperFunction;
    }

    private static OtherRevenue mapRow(FieldValueList row) {
        return new OtherRevenue(getField(row, "DATE_REQUEST"), getField(row, "DESC_SHORT_REQUEST_DAY"),
                getField(row, "ID_BRAND"),
                getField(row, "DESC_BRAND_ABBREVIATION"), getField(row, "ID_MARKET"),
                getField(row, "DESC_MARKET"), getField(row, "OTHERREVENUEOTHERPURCHASES"));
    }

    public void updateMap(Map<Revenue, String> updatedOtherRevenueMatcherMap) {
        revenueMap.clear();
        revenueMap.putAll(updatedOtherRevenueMatcherMap);
    }

    public String get(OtherRevenue other) {
        return revenueMap.get(other);

    }

    public BigDecimal getOtherRevenueAmount(BookingDetailsContainer bookingDetailsContainer) {
        return new BigDecimal(Optional.ofNullable(get(new OtherRevenue("", "", bookingDetailsContainer.getBrand(), "", bookingDetailsContainer.getMarket(), "", ""))).orElse("0"));
    }

    @Override
    public String getQuery() {
        return "SELECT * from `odigeo.com:api-project-452189918510.r_marketing_eu.OTHER_ORDER` where DATE(DATE_REQUEST) = (SELECT MAX(DATE(DATE_REQUEST)) FROM `odigeo.com:api-project-452189918510.r_marketing_eu.OTHER_ORDER`)";
    }

}
