package com.odigeo.marketing.revenueservice;

import com.google.cloud.bigquery.FieldValueList;
import com.google.inject.Singleton;
import com.odigeo.marketing.rmarketing.revenuemodels.Revenue;
import com.odigeo.marketing.rmarketing.revenuemodels.TaxRefundRevenue;
import com.odigeo.marketing.rmarketing.v1.BookingDetailsContainer;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

@Singleton
public class TaxRefundRevenueController extends RevenueController {

    private final Function<FieldValueList, Revenue> mapperFunction = (FieldValueList row) -> mapRow(row);

    public Function<FieldValueList, Revenue> getMapper() {
        return mapperFunction;
    }

    private static TaxRefundRevenue mapRow(FieldValueList row) {
        return new TaxRefundRevenue(getField(row, "DATE_REQUEST"), getField(row, "ID_WEBSITE"),
                getField(row, "ID_ITINERARY_HAUL_TYPE"),
                getField(row, "DESC_ITINERARY_HAUL_TYPE"),
                getField(row, "ID_PRODUCT_DETAIL"),
                getField(row, "ID_PROVIDER"),
                getField(row, "ID_VALIDATING_CARRIER"), getField(row, "OTHERREVENUETAXREFUNDSFLIGHTSE")
        );
    }

    public void updateMap(Map<Revenue, String> udpatedTaxRefundMatcherMap) {
        revenueMap.clear();
        revenueMap.putAll(udpatedTaxRefundMatcherMap);
    }

    public String get(TaxRefundRevenue other) {
        return revenueMap.get(other);

    }

    @Override
    public String getQuery() {
        return "SELECT * from `odigeo.com:api-project-452189918510.r_marketing_eu.TAXREFUND_SERVICE` where DATE(DATE_REQUEST) = (SELECT MAX(DATE(DATE_REQUEST)) FROM `odigeo.com:api-project-452189918510.r_marketing_eu.TAXREFUND_SERVICE`)";

    }


    public BigDecimal getTaxRefundAmount(BookingDetailsContainer bookingDetailsContainer) {
        return new BigDecimal(Optional.ofNullable(get(new TaxRefundRevenue("", bookingDetailsContainer.getWebsite(), bookingDetailsContainer.getHaulType(), "", bookingDetailsContainer.getProductDetail(), bookingDetailsContainer.getProvider(), bookingDetailsContainer.getCarrierValidating(), ""))).orElse("0"));
    }
}
