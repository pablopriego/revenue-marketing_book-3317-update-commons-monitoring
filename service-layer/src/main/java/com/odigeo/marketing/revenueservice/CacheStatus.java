package com.odigeo.marketing.revenueservice;

import com.edreams.configuration.RefreshableSingleton;

import java.util.concurrent.atomic.AtomicInteger;

@RefreshableSingleton
public class CacheStatus {
    AtomicInteger currentCacheStatus = new AtomicInteger(0);

    public int getAndSetCurrentCacheStatus() {
        return currentCacheStatus.getAndSet(1);
    }
}

