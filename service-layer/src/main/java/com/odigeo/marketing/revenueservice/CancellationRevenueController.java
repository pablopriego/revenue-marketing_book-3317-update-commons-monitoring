package com.odigeo.marketing.revenueservice;

import com.google.cloud.bigquery.FieldValueList;
import com.google.inject.Singleton;
import com.odigeo.marketing.rmarketing.revenuemodels.CancellationRevenue;
import com.odigeo.marketing.rmarketing.revenuemodels.Revenue;
import com.odigeo.marketing.rmarketing.v1.BookingDetailsContainer;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.function.Function;

@Singleton
public class CancellationRevenueController extends RevenueController {

    private final Function<FieldValueList, Revenue> mapperFunction = (FieldValueList row) -> mapRow(row);

    public CancellationRevenueController() {
    }

    public String getQuery() {
        return "SELECT * FROM `odigeo.com:api-project-452189918510.r_marketing_eu.CANCELLED_ORDER` WHERE DATE(DATE_REQUEST) = (SELECT MAX(DATE(DATE_REQUEST)) FROM `odigeo.com:api-project-452189918510.r_marketing_eu.CANCELLED_ORDER`)";
    }

    public String get(CancellationRevenue cancellation) {
        return revenueMap.get(cancellation);

    }

    public BigDecimal getCancellationAmount(BookingDetailsContainer bookingDetailsContainer) {
        return new BigDecimal(Optional.ofNullable(this.get(new CancellationRevenue("", bookingDetailsContainer.getBrand(), bookingDetailsContainer.getMarket(), bookingDetailsContainer.getHaulType(), "", bookingDetailsContainer.getProductDetail(), ""))).orElse("0"));
    }

    @Override
    public Function<FieldValueList, Revenue> getMapper() {
        return mapperFunction;
    }

    private static CancellationRevenue mapRow(FieldValueList row) {
        return new CancellationRevenue(getField(row, "DATE_REQUEST"), getField(row, "ID_BRAND"),
                getField(row, "ID_MARKET"),
                getField(row, "ID_ITINERARY_HAUL_TYPE"),
                getField(row, "DESC_ITINERARY_HAUL_TYPE"),
                getField(row, "ID_PRODUCT_DETAIL"),
                getField(row, "OTHERREVENUECANCELLATIONPURCHA")
        );
    }

}
