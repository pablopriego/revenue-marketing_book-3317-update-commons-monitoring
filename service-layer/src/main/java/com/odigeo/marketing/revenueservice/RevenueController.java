package com.odigeo.marketing.revenueservice;

import com.google.cloud.bigquery.FieldValueList;
import com.google.cloud.bigquery.TableResult;
import com.odigeo.marketing.rmarketing.revenuemodels.Revenue;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

public abstract class RevenueController {

    Map<Revenue, String> revenueMap = new ConcurrentHashMap<>();

    public abstract String getQuery();

    public abstract Function<FieldValueList, Revenue> getMapper();


    public void updateMap(Map<Revenue, String> updatedCancellationIssuedMap) {
        revenueMap.clear();
        revenueMap.putAll(updatedCancellationIssuedMap);
    }

    public Map<Revenue, String> buildAll(Function<FieldValueList, Revenue> mapper,
                                                TableResult result) {
        Map<Revenue, String> revenueMap = new ConcurrentHashMap<>();
        for (FieldValueList row : result.iterateAll()) {
            Revenue revenue = mapper.apply(row);
            revenueMap.put(revenue, revenue.getRevenueValue());
        }
        return revenueMap;
    }

    protected static String getField(FieldValueList row, String fieldName) {
        return (String) Optional.ofNullable(row)
                .map(r -> r.get(fieldName))
                .map(value -> value.getValue()).orElse("");
    }



    public  void update(TableResult result) {
        Map<Revenue, String> map = this.buildAll(this.getMapper(), result);
        updateMap(map);
    }
}
