package com.odigeo.marketing.revenueservice;

import com.google.cloud.bigquery.FieldValueList;
import com.google.inject.Singleton;
import com.odigeo.marketing.rmarketing.revenuemodels.CCIncentiveRevenue;
import com.odigeo.marketing.rmarketing.revenuemodels.Revenue;
import com.odigeo.marketing.rmarketing.v1.BookingDetailsContainer;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

@Singleton
public class CCIncentiveRevenueController extends RevenueController {

    private final Function<FieldValueList, Revenue> mapperFunction = (FieldValueList row) -> mapRow(row);

    public Function<FieldValueList, Revenue> getMapper() {
        return mapperFunction;
    }

    private static CCIncentiveRevenue mapRow(FieldValueList row) {
        return new CCIncentiveRevenue(getField(row, "DATE_REQUEST"), getField(row, "ID_BRAND"),
                getField(row, "ID_MARKET"),
                getField(row, "GROSSSALESPURCHASES"));
    }

    @Override
    public void updateMap(Map<Revenue, String> updatedCCIncentiveMatcherMap) {
        revenueMap.clear();
        revenueMap.putAll(updatedCCIncentiveMatcherMap);
    }

    public BigDecimal getCCIncentiveAmount(BookingDetailsContainer bookingDetailsContainer) {
        return new BigDecimal(Optional.ofNullable(this.get(new CCIncentiveRevenue("", bookingDetailsContainer.getBrand(), bookingDetailsContainer.getMarket(), ""))).orElse("0"));
    }


    public String get(CCIncentiveRevenue ccIncentive) {
        return revenueMap.get(ccIncentive);

    }

    @Override
    public String getQuery() {
        return "SELECT * from `odigeo.com:api-project-452189918510.r_marketing_eu.INCENTIVE_ORDER` where DATE(DATE_REQUEST) = (SELECT MAX(DATE(DATE_REQUEST)) FROM `odigeo.com:api-project-452189918510.r_marketing_eu.INCENTIVE_ORDER`)";
    }
}
