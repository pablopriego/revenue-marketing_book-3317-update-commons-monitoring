package com.odigeo.marketing.revenue.monitoring;

import com.odigeo.marketing.rmarketing.check.RevenueMarketingSelfCheck;
import org.apache.log4j.Logger;

import javax.ejb.Local;
import javax.ejb.Stateless;
@SuppressWarnings("PMD")
@Stateless
@Local(MonitoringService.class)
public class MonitoringServiceBean implements MonitoringService {

    private static final Logger LOGGER = Logger.getLogger(MonitoringServiceBean.class);


    @Override
    public ServiceStatus runSelfCheck(RevenueMarketingSelfCheck revenueMarketingSelfCheck, String usualFlowErrorMessage) {
        ServiceStatus serviceStatus = new ServiceStatus();
        runSelfCheckWithUsualFlow(serviceStatus, revenueMarketingSelfCheck, usualFlowErrorMessage);
        return serviceStatus;
    }

    private void runSelfCheckWithUsualFlow(ServiceStatus serviceStatus,
                                           RevenueMarketingSelfCheck revenueMarketingSelfCheck, String errorMessage) {
        //revenueMarketingSelfCheck.selfCheckUsualFlow();
        serviceStatus.setResponseObtained(true);

    }

}
