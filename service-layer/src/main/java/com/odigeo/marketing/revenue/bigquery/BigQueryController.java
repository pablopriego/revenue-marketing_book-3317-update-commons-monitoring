package com.odigeo.marketing.revenue.bigquery;

import com.edreams.configuration.ConfiguredInPropertiesFile;
import com.google.auth.oauth2.ServiceAccountCredentials;

import com.google.cloud.bigquery.BigQuery;
import com.google.cloud.bigquery.QueryJobConfiguration;
import com.google.cloud.bigquery.BigQueryOptions;
import com.google.cloud.bigquery.TableResult;
import com.google.cloud.bigquery.Job;
import com.google.cloud.bigquery.JobId;
import com.google.cloud.bigquery.JobInfo;
import com.odigeo.marketing.revenueservice.RevenueManager;
import org.apache.log4j.Logger;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

@ConfiguredInPropertiesFile
public class BigQueryController {

    private BigQuery bigQueryService;
    static String projectID = "odigeo.com:api-project-452189918510";
    private String keyLocation;
    private static final Logger LOGGER = Logger.getLogger(RevenueManager.class);

    public void setKeyLocation(String keyLocation) {
        this.keyLocation = keyLocation;
    }

    public BigQuery getService() {
        InputStream is = null;
        if (keyLocation != null) {
            is = new ByteArrayInputStream(keyLocation.getBytes(StandardCharsets.UTF_8));
        }
        try {
            return bigQueryService = BigQueryOptions.newBuilder().setProjectId(projectID)
                    .setCredentials(
                            ServiceAccountCredentials.fromStream(is)
                    ).build().getService();
        } catch (IOException e) {
            LOGGER.info("Unable to obtain the BigQuery service: " + e.getMessage());
        }
        return null;
    }

    public TableResult createAndRunJob(QueryJobConfiguration queryJobConfiguration) throws InterruptedException {
        JobId jobId = JobId.of(UUID.randomUUID().toString());
        Job queryJob;
        if (bigQueryService != null) {
            queryJob = bigQueryService.create(JobInfo.newBuilder(queryJobConfiguration).setJobId(jobId).build());
            queryJob = queryJob.waitFor();
            verifyQueryJob(queryJob);
            return queryJob.getQueryResults();
        } // Create exception here
        return null;
    }

    public QueryJobConfiguration getQueryJobConfiguration(String cancelModifPerOrder) {
        return QueryJobConfiguration.newBuilder(
                cancelModifPerOrder)
                .setUseLegacySql(false)
                .build();
    }

    private void verifyQueryJob(Job queryJob) {
        if (queryJob == null) {
            LOGGER.info("BigQuery failed to load");
        } else if (queryJob.getStatus().getError() != null) {
            LOGGER.info("BigQuery job failed due to: " + queryJob.getStatus().getError().toString());
        }
    }


    public TableResult getResults(String query) throws InterruptedException {
        return createAndRunJob(getQueryJobConfiguration(query));
    }
}


