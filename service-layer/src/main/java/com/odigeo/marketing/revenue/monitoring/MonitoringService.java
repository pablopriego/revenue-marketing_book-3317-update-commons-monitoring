package com.odigeo.marketing.revenue.monitoring;


import com.odigeo.marketing.rmarketing.check.RevenueMarketingSelfCheck;

public interface MonitoringService {

    ServiceStatus runSelfCheck(RevenueMarketingSelfCheck revenueMarketingSelfCheck, String usualFlowErrorMessage);
}
