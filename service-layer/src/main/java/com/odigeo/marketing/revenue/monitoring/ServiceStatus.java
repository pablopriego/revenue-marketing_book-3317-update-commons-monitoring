package com.odigeo.marketing.revenue.monitoring;

public class ServiceStatus {

    private boolean responseObtained;

    public ServiceStatus() {
        responseObtained = false;
    }

    public boolean isResponseObtained() {
        return responseObtained;
    }

    public void setResponseObtained(boolean responseObtained) {
        this.responseObtained = responseObtained;
    }
}
