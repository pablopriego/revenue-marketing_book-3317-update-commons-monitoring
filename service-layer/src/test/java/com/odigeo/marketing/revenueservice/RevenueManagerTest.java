package com.odigeo.marketing.revenueservice;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.marketing.revenue.bigquery.BigQueryController;
import com.odigeo.marketing.rmarketing.v1.BookingDetailsContainer;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RevenueManagerTest {

    @Mock
    BigQueryController bigQueryController;
    @Mock
    CacheStatus cacheStatus;
    @Mock
    CancellationRevenueController cancellationRevenueController;
    @Mock
    CCIncentiveRevenueController ccIncentiveRevenueController;
    @Mock
    OtherRevenueController otherRevenueController;
    @Mock
    TaxRefundRevenueController taxRefundRevenueController;


    BookingDetailsContainer bookingDetailsContainer;

    @BeforeClass
    public void setup() {
        bookingDetailsContainer = new BookingDetailsContainer(null, "ES", "ES", "1", "", "", "", "");
        ConfigurationEngine.init();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testUpdatesControllerCachesIfCacheStatusZero() throws InterruptedException {
        RevenueManager revenueManager = new RevenueManager(bigQueryController, cacheStatus, cancellationRevenueController, ccIncentiveRevenueController, otherRevenueController, taxRefundRevenueController);
        when(cacheStatus.getAndSetCurrentCacheStatus()).thenReturn(0);
        revenueManager.getRevenue(bookingDetailsContainer);
        verify(cancellationRevenueController).getCancellationAmount(bookingDetailsContainer);
        verify(ccIncentiveRevenueController).getCCIncentiveAmount(bookingDetailsContainer);
        verify(otherRevenueController).getOtherRevenueAmount(bookingDetailsContainer);
        verify(taxRefundRevenueController).getTaxRefundAmount(bookingDetailsContainer);

    }

    @Test
    public void testNullBookingDetailsContainer() throws InterruptedException {
/*        bookingDetailsContainer = null;
        RevenueManager revenueManager = new RevenueManager(bigQueryController, cacheStatus, cancellationRevenueController, ccIncentiveRevenueController, otherRevenueController, taxRefundRevenueController);
        when(cacheStatus.getAndSetCurrentCacheStatus()).thenReturn(0);
        RevenueMarketingAdjustmentContainer adjustmentContainer = revenueManager.getRevenue(bookingDetailsContainer);
        verify(cancellationRevenueController).getCancellationAmount(bookingDetailsContainer);
        verify(ccIncentiveRevenueController).getCCIncentiveAmount(bookingDetailsContainer);
        verify(otherRevenueController).getOtherRevenueAmount(bookingDetailsContainer);
        verify(taxRefundRevenueController).getTaxRefundAmount(bookingDetailsContainer);
        Assert.assertEquals(adjustmentContainer.getCancelModify(), null); //fix*/
    }
}
