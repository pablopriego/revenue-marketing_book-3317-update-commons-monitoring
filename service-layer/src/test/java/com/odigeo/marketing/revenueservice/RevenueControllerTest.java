package com.odigeo.marketing.revenueservice;

import com.google.cloud.bigquery.FieldValueList;
import com.google.cloud.bigquery.TableResult;
import com.odigeo.marketing.rmarketing.revenuemodels.CancellationRevenue;
import com.odigeo.marketing.rmarketing.revenuemodels.Revenue;
import com.odigeo.marketing.rmarketing.v1.BookingDetailsContainer;
import org.mockito.Mock;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.HashMap;

public class RevenueControllerTest {

    CancellationRevenueController revenueController;
    BookingDetailsContainer bookingDetailsContainer = new BookingDetailsContainer("", "ES", "ES", "1", "", "", "", "");
    CancellationRevenue cancellationRevenue = new CancellationRevenue("", "ES", "ES", "1", "", "", "1.00");

    @Mock
    TableResult tableResult;
    FieldValueList fieldValueList;

    @BeforeClass
    public void setup() {

    }

    @Test
    public void testUpdateMap() {
        HashMap emptyMap = new HashMap<Revenue, String>();
        HashMap updatedMap = new HashMap<Revenue, String>();
        revenueController = new CancellationRevenueController();
        revenueController.updateMap(emptyMap);
        updatedMap.put(cancellationRevenue, "");
        revenueController.updateMap(emptyMap);
        revenueController.updateMap(updatedMap);
    }

    @Test
    public void testBuildAll() {
    }


}
