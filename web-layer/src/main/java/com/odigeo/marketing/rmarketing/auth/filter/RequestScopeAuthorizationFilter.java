package com.odigeo.marketing.rmarketing.auth.filter;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Singleton;
import com.odigeo.marketing.rmarketing.auth.RestRequestHolder;

import javax.servlet.Filter;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Singleton
public class RequestScopeAuthorizationFilter implements Filter {

    private static final String TOKEN_FIELD = "userToken";

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        getSecurityInfo(request);
        chain.doFilter(request, response);
    }

    private void getSecurityInfo(ServletRequest servletRequest) {
        HttpServletRequest request = (HttpServletRequest) servletRequest;

        final String token = fetchUserToken(request);
        initRestRequestHolder(token, request.getPathInfo(), request.getMethod());
    }

    private void initRestRequestHolder(String token, String resource, String action) {
        RestRequestHolder restRequest = ConfigurationEngine.getInstance(RestRequestHolder.class);
        restRequest.setUserToken(token);
        restRequest.setResource(resource);
        restRequest.setAction(action);
    }

    private String fetchUserToken(HttpServletRequest request) {
        return request.getHeader(TOKEN_FIELD);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        //do nothing
    }

    @Override
    public void destroy() {
        //do nothing
    }
}
