package com.odigeo.marketing.rmarketing.healthcheck;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public interface IGenericHealthChecker {
    boolean checkConnection(HttpURLConnection connection) throws IOException;
    URL getURL() throws MalformedURLException;
}
