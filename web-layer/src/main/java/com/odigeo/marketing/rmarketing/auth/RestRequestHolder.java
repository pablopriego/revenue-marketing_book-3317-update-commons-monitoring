package com.odigeo.marketing.rmarketing.auth;

import com.google.inject.servlet.RequestScoped;

@RequestScoped
public class RestRequestHolder {

    private String userToken;
    private String resource;
    private String action;


    public String getResource() {
        return resource;
    }

    public String getAction() {
        return action;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
