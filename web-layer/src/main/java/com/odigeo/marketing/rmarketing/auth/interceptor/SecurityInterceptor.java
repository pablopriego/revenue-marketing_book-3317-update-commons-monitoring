package com.odigeo.marketing.rmarketing.auth.interceptor;


import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.marketing.rmarketing.auth.AuthorizationController;
import com.odigeo.marketing.rmarketing.auth.RestRequestHolder;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

import java.lang.reflect.Method;
import java.util.Optional;

public class SecurityInterceptor implements MethodInterceptor {

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {

        Optional<SecuredMethod> securedMethod =  getSecuredMethod(invocation.getMethod());

        securedMethod.map(SecuredMethod::getResource)
                .map(this::getRestRquestHolder)
                .ifPresent(this::authorize);

        return invocation.proceed();
    }

    private Optional<SecuredMethod> getSecuredMethod(Method method) {
        return new SecuredMethod.Builder().method(method).build();
    }

    private void authorize(RestRequestHolder restRequestHolder) {
        ConfigurationEngine.getInstance(AuthorizationController.class).authorize(restRequestHolder);
    }

    private RestRequestHolder getRestRquestHolder(String resource) {
        RestRequestHolder restRequestHolder = ConfigurationEngine.getInstance(RestRequestHolder.class);
        restRequestHolder.setResource(resource);
        return restRequestHolder;
    }
}
