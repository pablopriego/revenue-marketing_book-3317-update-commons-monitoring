package com.odigeo.marketing.rmarketing.v1;

import com.google.inject.Inject;
import com.odigeo.marketing.revenueservice.RevenueManager;
import com.odigeo.marketing.rmarketing.v1.response.RevenueMarketingAdjustmentContainer;

public class RevenueMarketingServiceController implements RevenueMarketingService {

    private final RevenueManager revenueManager;

    @Inject
    public RevenueMarketingServiceController(RevenueManager revenueManager) {
        this.revenueManager = revenueManager;
    }


    @Override
    public RevenueMarketingAdjustmentContainer getRevenue(BookingDetailsContainer bookingDetailsContainer) {
        return revenueManager.getRevenue(bookingDetailsContainer);
    }

}
