package com.odigeo.marketing.rmarketing.auth;


public class AuthorizationMessages {

    static final String INVALID_TOKEN_NULL = "Invalid user token: null";
    static final String INVALID_RESOURCE_PATH_NULL = "Invalid Resurce path: null ";
    static final String UNAUTHORIZED_USER = "User doesn't have permission ";
    static final String AUTHORIZATION_REQUEST_INFO_NULL = "Request info can't be null ";

}
