package com.odigeo.marketing.rmarketing.healthcheck;

import com.codahale.metrics.health.HealthCheck;
import com.google.inject.Inject;
import com.odigeo.marketing.revenue.bigquery.BigQueryController;

public class BigQueryConectivityCheck extends HealthCheck {

    public static final String BIG_QUERY_SELF_CHECK = "BigQueryConnection";


    BigQueryController bigQueryController;

    @Inject
    public BigQueryConectivityCheck(BigQueryController bigQueryController) {
        this.bigQueryController = bigQueryController;
    }

    @Override
    protected Result check() {
        if (bigQueryController.getService() != null) {
            return Result.healthy("Connected to BigQuery");
        } else {
            return Result.unhealthy("error : No Connection");
        }
    }
}

