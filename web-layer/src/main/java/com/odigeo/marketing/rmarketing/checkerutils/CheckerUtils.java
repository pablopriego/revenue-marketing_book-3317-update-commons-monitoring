package com.odigeo.marketing.rmarketing.checkerutils;

import com.codahale.metrics.health.HealthCheck;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.marketing.revenue.monitoring.MonitoringService;
import com.odigeo.marketing.revenue.monitoring.ServiceStatus;

public class CheckerUtils {

    public static MonitoringService getAvailabilityService() {
        return ConfigurationEngine.getInstance(MonitoringService.class);
    }

    public static String getDescription(ServiceStatus serviceStatus) {
        return new StringBuilder("Usual flow: ").append(serviceStatus.isResponseObtained() ? "OK" : "KO")
                .toString();
    }

    public static HealthCheck.Result healthStatus(ServiceStatus serviceStatus) {
        if (CheckerUtils.isHealthy(serviceStatus)) {
            return HealthCheck.Result.healthy(CheckerUtils.getDescription(serviceStatus));
        } else {
            return HealthCheck.Result.unhealthy(CheckerUtils.getDescription(serviceStatus));
        }
    }

    private static boolean isHealthy(ServiceStatus serviceStatus) {
        return serviceStatus.isResponseObtained();
    }
}
