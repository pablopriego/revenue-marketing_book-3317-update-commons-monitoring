package com.odigeo.marketing.rmarketing.auth;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.authservices.authorization.api.v1.AuthServicesAuthorizationApiService;
import com.odigeo.authservices.authorization.api.v1.exceptions.UnauthorizedException;
import com.odigeo.authservices.authorization.api.v1.response.GetPermissionResponse;
import org.apache.log4j.Logger;

@Singleton
public class AuthorizationController {

    private static final Logger logger = Logger.getLogger(AuthorizationController.class);

    private final AuthServicesAuthorizationApiService authServicesAuthorizationApiService;
    private final AuthorizationConfiguration authorizationConfiguration;
    private final AuthorizationHelper authorizationHelper;


    @Inject
    public AuthorizationController(AuthServicesAuthorizationApiService authServicesAuthorizationApiService, AuthorizationConfiguration authorizationConfiguration, AuthorizationHelper authorizationHelper) {
        this.authServicesAuthorizationApiService = authServicesAuthorizationApiService;
        this.authorizationConfiguration = authorizationConfiguration;
        this.authorizationHelper = authorizationHelper;
    }

    public void authorize(RestRequestHolder restRequestHolder) {
        if (authorizationConfiguration.isSecurityEnabled()) {
            doAuthorization(restRequestHolder);
        }
    }

    private void doAuthorization(RestRequestHolder restRequestHolder) {
        verifyRestRequest(restRequestHolder);

        if (!authorizationHelper.isAuthorized(getPermissions(restRequestHolder.getUserToken()), restRequestHolder)) {
            throw new UnauthorizedException(AuthorizationMessages.UNAUTHORIZED_USER);
        }
    }

    private void verifyRestRequest(RestRequestHolder restRequestHolder) {
        if (restRequestHolder == null) {
            UnauthorizedException unauthorizedException = new UnauthorizedException(AuthorizationMessages.AUTHORIZATION_REQUEST_INFO_NULL);
            logger.error(unauthorizedException);
            throw unauthorizedException;
        }
        if (restRequestHolder.getUserToken() == null) {
            throw new UnauthorizedException(AuthorizationMessages.INVALID_TOKEN_NULL);
        }
        if (restRequestHolder.getResource() == null) {
            throw new UnauthorizedException(AuthorizationMessages.INVALID_RESOURCE_PATH_NULL);
        }
    }

    private GetPermissionResponse getPermissions(String userToken) {
        return authServicesAuthorizationApiService.getTokenPermissions(authorizationConfiguration.getAppId(), userToken);
    }
}
