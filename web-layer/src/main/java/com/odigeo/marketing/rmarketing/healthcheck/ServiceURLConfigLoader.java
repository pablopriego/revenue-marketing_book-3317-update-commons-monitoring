package com.odigeo.marketing.rmarketing.healthcheck;

import com.odigeo.commons.rest.configuration.URLConfiguration;
import com.odigeo.commons.rest.configuration.URLConfigurationLoader;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;

public class ServiceURLConfigLoader {
    private static final Logger LOGGER = Logger.getLogger(ServiceURLConfigLoader.class);

    public static String getUrlConfiguration(Class service) throws MalformedURLException {
        URLConfiguration urlConfiguration = null;
        try {
            urlConfiguration = new URLConfigurationLoader().getURLConfiguration(service);
        } catch (IOException | InvocationTargetException | IllegalAccessException e) {
            LOGGER.error("Error loading service URL from properties", e);
        }
        return urlConfiguration != null ? urlConfiguration.getUri().toString() : StringUtils.EMPTY;
    }
}
