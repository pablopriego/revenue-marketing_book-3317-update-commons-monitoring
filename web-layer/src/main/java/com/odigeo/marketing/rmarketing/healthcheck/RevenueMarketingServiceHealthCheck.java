package com.odigeo.marketing.rmarketing.healthcheck;

import com.codahale.metrics.health.HealthCheck;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.marketing.revenue.monitoring.ServiceStatus;
import com.odigeo.marketing.rmarketing.checkerutils.CheckerUtils;
import com.odigeo.marketing.rmarketing.check.RevenueMarketingSelfCheck;

@Singleton
public class RevenueMarketingServiceHealthCheck extends HealthCheck {

    private static final String REVENUE_MARKETING_SERVICE_SELF_CHECK_ERROR_MESSAGE = "[Self-Check]Error getting rmarketing service: "; // todo : remove
    public static final String REVENUE_MARKETING_SELF_CHECK = "MetasearchEngine Service";
    @Inject
    public RevenueMarketingServiceHealthCheck() {

    }

    @Override
    protected Result check() {
        ServiceStatus serviceStatus = CheckerUtils.getAvailabilityService().runSelfCheck(getRevenueMarketingSelfCheck(),
                REVENUE_MARKETING_SERVICE_SELF_CHECK_ERROR_MESSAGE);
        return CheckerUtils.healthStatus(serviceStatus);
    }


    private RevenueMarketingSelfCheck getRevenueMarketingSelfCheck() {
        return ConfigurationEngine.getInstance(RevenueMarketingSelfCheck.class);
    }

}
