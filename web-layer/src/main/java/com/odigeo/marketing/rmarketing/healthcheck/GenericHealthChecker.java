package com.odigeo.marketing.rmarketing.healthcheck;

import com.codahale.metrics.health.HealthCheck;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public abstract class GenericHealthChecker extends HealthCheck implements IGenericHealthChecker {
    private static final int CONNECT_TIMEOUT = 60 * 1000;
    private static final int READ_TIMEOUT = 10 * 60 * 1000;
    private static final String HTTP_VERB = "GET";

    public Result check(final URL url, final Logger logger) {
        URLConnection connection = null;

        try {
            connection = url.openConnection();
            if (connection instanceof HttpURLConnection) {
                HttpURLConnection httpURLConnection = (HttpURLConnection) connection;
                if (checkConnection(httpURLConnection)) {
                    return Result.healthy();
                }
            }
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return Result.unhealthy(e);
        } finally {
            if (connection instanceof HttpURLConnection) {
                ((HttpURLConnection) connection).disconnect();
            }
        }
        return Result.unhealthy(this.getClass().getSimpleName());
    }

    @Override
    public boolean checkConnection(HttpURLConnection connection) throws IOException {
        connection.setConnectTimeout(CONNECT_TIMEOUT);
        connection.setReadTimeout(READ_TIMEOUT);
        connection.setRequestMethod(HTTP_VERB);
        connection.connect();
        return connection.getResponseCode() == HttpURLConnection.HTTP_OK;
    }
}
