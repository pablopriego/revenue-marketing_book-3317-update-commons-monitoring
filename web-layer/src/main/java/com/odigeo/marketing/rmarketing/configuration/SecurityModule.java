package com.odigeo.marketing.rmarketing.configuration;

import com.google.inject.AbstractModule;
import com.google.inject.matcher.Matchers;
import com.odigeo.marketing.rmarketing.auth.interceptor.Secured;
import com.odigeo.marketing.rmarketing.auth.interceptor.SecurityInterceptor;


public class SecurityModule extends AbstractModule {


    @Override
    protected void configure() {
        bindInterceptor(Matchers.any(), Matchers.annotatedWith(Secured.class), new SecurityInterceptor());
    }
}

