package com.odigeo.marketing.rmarketing.auth.interceptor;


import java.lang.reflect.Method;
import java.util.Optional;

public class SecuredMethod {

    private final String resource;

    SecuredMethod(String resource) {
        this.resource = resource;
    }

    public String getResource() {
        return resource;
    }

    public static class Builder {
        private Method method;

        public Builder method(Method method) {
            this.method = method;
            return this;
        }

        public Optional<SecuredMethod> build() {
            if (method == null || method.getAnnotation(Secured.class) == null) {
                return Optional.empty();
            }
            SecuredMethod securedMethod = new SecuredMethod(method.getAnnotation(Secured.class).resource());
            return Optional.of(securedMethod);
        }
    }

}
