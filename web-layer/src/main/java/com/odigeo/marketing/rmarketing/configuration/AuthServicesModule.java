package com.odigeo.marketing.rmarketing.configuration;

import com.odigeo.authservices.authorization.api.v1.AuthServicesAuthorizationApiService;
import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.guice.AbstractRestUtilsModule;
import com.odigeo.commons.rest.guice.configuration.JacksonVersion;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;

public class AuthServicesModule extends AbstractRestUtilsModule<AuthServicesAuthorizationApiService> {

    public AuthServicesModule(ServiceNotificator... serviceNotificators) {
        super(AuthServicesAuthorizationApiService.class, serviceNotificators);
    }

    @Override
    protected ServiceConfiguration<AuthServicesAuthorizationApiService> getServiceConfiguration(Class<AuthServicesAuthorizationApiService> aClass) {

        ConnectionConfiguration connectionConfiguration = new ConnectionConfiguration();
        connectionConfiguration.setSocketTimeoutInMillis(15000);
        connectionConfiguration.setConnectionTimeoutInMillis(15000);

        ServiceConfiguration.Builder<AuthServicesAuthorizationApiService> authConfiguration = new ServiceConfiguration.Builder<>(AuthServicesAuthorizationApiService.class).withJacksonVersion(JacksonVersion.V_1_9_9).withConnectionConfiguration(connectionConfiguration);

        return authConfiguration.build();
    }
}

