package com.odigeo.marketing.rmarketing.rest;

import com.edreams.configuration.ConfigurationEngine;

import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

public class ServiceApplication extends Application {

    private final Set<Object> singletons = new HashSet<Object>();

    /**
     * Register all REST services here.
     *
     * @since 1.0
     */
    public ServiceApplication() {
        singletons.add(ConfigurationEngine.getInstance(com.odigeo.marketing.rmarketing.v1.RevenueMarketingServiceController.class));
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }
}

