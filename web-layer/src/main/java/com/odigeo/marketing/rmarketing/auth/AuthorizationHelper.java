package com.odigeo.marketing.rmarketing.auth;

import com.google.inject.Singleton;
import com.odigeo.authservices.authorization.api.v1.pojos.ParametrizedPermission;
import com.odigeo.authservices.authorization.api.v1.response.GetPermissionResponse;

import java.util.Set;

@Singleton
public class AuthorizationHelper {

    boolean isAuthorized(GetPermissionResponse userPermissions, RestRequestHolder restRequestHolder) {
        if (userPermissions == null || userPermissions.getPerAppPermissions() == null || userPermissions.getPerAppPermissions().getPermissions() == null || restRequestHolder == null) {
            return false;
        }
        return checkPermission(userPermissions.getPerAppPermissions().getPermissions(), restRequestHolder);
    }

    private boolean checkPermission(Set<ParametrizedPermission> userResourcePermissions, RestRequestHolder restRequestHolder) {
        return userResourcePermissions.stream()
                .filter(permission -> permission.getSubject().equals(restRequestHolder.getResource()))
                .anyMatch(userResourcePermission -> userResourcePermission.getAction().equals(restRequestHolder.getAction()));
    }

}

