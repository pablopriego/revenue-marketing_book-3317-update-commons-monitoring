package com.odigeo.marketing.rmarketing.bootstrap;

import com.codahale.metrics.health.HealthCheckRegistry;
import com.edreams.configuration.ConfigurationEngine;
import com.edreams.configuration.ConfigurationFilesManager;
import com.odigeo.authservices.authorization.api.v1.AuthServicesAuthorizationApiService;
import com.odigeo.commons.monitoring.dump.DumpServlet;
import com.odigeo.commons.monitoring.dump.DumpStateRegistry;
import com.odigeo.commons.monitoring.healthcheck.HealthCheckServlet;
import com.odigeo.commons.monitoring.metrics.MetricsManager;
import com.odigeo.commons.monitoring.metrics.configuration.MonitoringConfigurationFilesManager;
import com.odigeo.commons.monitoring.metrics.reporter.ReporterStatus;
import com.odigeo.commons.rest.configuration.ServiceConfigurationFilesManager;
import com.odigeo.commons.rest.monitoring.dump.ServiceDumpState;
import com.odigeo.marketing.rmarketing.healthcheck.BigQueryConectivityCheck;
import com.odigeo.util.jboss.VfsChecker;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.jboss.vfs.VFS;
import org.jboss.vfs.VFSUtils;
import org.jboss.vfs.VirtualFile;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.IOException;
import java.net.URL;

@SuppressWarnings("PMD")
public class ContextListener implements ServletContextListener {

    private static final Logger logger = Logger.getLogger(ContextListener.class);
    public static final String APPLICATION_NAME = "revenue-marketing";
    private static final String CONFIGURATION_ENGINE = "Revenue Marketing Service: ConfigurationEngine has been initialized";
    private static final String LOG4J_FILENAME = "/log4j.properties";
    private static final long LOG4J_WATCH_DELAY_MS = 1800000L;

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        logger.info("Bootstrapping .....");

        initPropertiesInExternalFolder();

        initPropertiesFileBasedConfigurator();

        initLog4J(servletContextEvent);

        initConfigurationEngine();

        initMonitoringServlets(servletContextEvent.getServletContext());

        initMonitoringMetrics();

        logger.info("Bootstrapping finished!");
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        event.getServletContext().log("context destroyed");
    }

    private void initPropertiesInExternalFolder() {
        ConfigurationFilesManager.setAppName(APPLICATION_NAME);
        ServiceConfigurationFilesManager.setAppName(APPLICATION_NAME);
        MonitoringConfigurationFilesManager.setAppName(APPLICATION_NAME);
    }

    private void initPropertiesFileBasedConfigurator() {
        ConfigurationFilesManager.setAppName(APPLICATION_NAME);
    }

    private void initConfigurationEngine() {
        ConfigurationEngine.init();
        logger.info("ConfigurationEngine has been initialized");
    }

    private void initLog4J(ServletContextEvent event) {
        if (VfsChecker.isVfsAvailable()) {
            VirtualFile virtualFile = VFS.getChild(ConfigurationFilesManager.getConfigurationFileUrl(LOG4J_FILENAME, this.getClass()).getFile());
            try {
                URL fileRealURL = VFSUtils.getPhysicalURL(virtualFile);
                PropertyConfigurator.configureAndWatch(fileRealURL.getFile(), LOG4J_WATCH_DELAY_MS);
                event.getServletContext().log("Log4j has been initialized with config file " + fileRealURL.getFile() + " and watch delay of " + (LOG4J_WATCH_DELAY_MS / 1000) + " seconds");
            } catch (IOException e) {
                event.getServletContext().log("Log4j cannot be initialized: file " + LOG4J_FILENAME + " cannot be load");
            }
        } else {
            final URL log4jFileLocation = ConfigurationFilesManager.getConfigurationFileUrl(LOG4J_FILENAME, this.getClass());
            if (log4jFileLocation == null) {
                event.getServletContext().log("Log4j cannot be initialized: file " + LOG4J_FILENAME + " cannot be found");
            } else {
                PropertyConfigurator.configureAndWatch(log4jFileLocation.getFile(), LOG4J_WATCH_DELAY_MS);
                event.getServletContext().log("Log4j has been initialized with config file " + log4jFileLocation.getFile() + " and watch delay of " + (LOG4J_WATCH_DELAY_MS / 1000) + " seconds");
            }
        }
    }

    private void initMonitoringServlets(ServletContext servletContext) {
        final HealthCheckRegistry healthCheckRegistry = new HealthCheckRegistry();
        healthCheckRegistry.register(BigQueryConectivityCheck.BIG_QUERY_SELF_CHECK, ConfigurationEngine.getInstance(BigQueryConectivityCheck.class));

        final DumpStateRegistry dumpStateRegistry = new DumpStateRegistry();
        dumpStateRegistry.add(new ServiceDumpState(AuthServicesAuthorizationApiService.class));

        servletContext.setAttribute(DumpServlet.REGISTRY_KEY, dumpStateRegistry);
        servletContext.setAttribute(HealthCheckServlet.REGISTRY_KEY, healthCheckRegistry);
    }

    private void initMonitoringMetrics() {
        MetricsManager.getInstance().addMetricsReporter(APPLICATION_NAME);
        MetricsManager.getInstance().changeReporterStatus(ReporterStatus.STARTED);
    }
}
