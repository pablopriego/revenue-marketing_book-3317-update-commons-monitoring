# Revenue Marketing Service


Marketing Channel Service contains the implementation of the Revenue Marketing Service Contract methods

* getRevenue

# getRevenue

This operation will return the revenue values to apply to a booking

* It receives an BookingDetailsContainer which contains the booking information relevent to revenue calculation
* It returns a RevenueMarketingAdjustmentContainer with 4 big decimal values to apply to a booking
