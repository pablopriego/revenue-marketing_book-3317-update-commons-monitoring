package com.odigeo.marketing.rmarketing;

import com.google.inject.Singleton;
import com.odigeo.technology.docker.ContainerException;
import com.odigeo.technology.docker.DockerHostRetriever;

@SuppressWarnings("PMD")
@Singleton
public class ServerConfiguration {

    public static final String ENGINEERING_CONTEXT = "/revenue-marketing/engineering/";

    private final DockerHostRetriever dockerHostRetriever;


    public ServerConfiguration() {
        dockerHostRetriever = new DockerHostRetriever();
    }

    public String getServer() throws ContainerException {
        return dockerHostRetriever.getDockerHost() + ":8080";
    }
}
